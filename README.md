# UDA_ACADEMY PROJECT

### 0. VIEW PRODUCTION OF THIS PROJECT
https://acacou.vercel.app/

### 1. CLONE THIS PROJECT

```bash
git clone https://gitlab.com/luan95194/uda-academy.git
```

### 2. RUN SCSS

```bash
sass ./style.scss ./style.css --watch
```
