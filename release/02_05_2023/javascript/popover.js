$(document).ready(function () {
  const cards = $('.card');
  const popover = $('.popover');

  cards.mouseenter(function (e) {
    if (this) {
      const cardId = $(this).attr('data-card');
      const cardRect = $(this)[0].getBoundingClientRect();
      const carHaltHeight = cardRect.height / 4;
      const popoverById = $(`.popover[data-card="${cardId}`);
      const popoverTop = cardRect.top + window.pageYOffset;
      let popoverLeft = cardRect.right;

      const screenWidth = window.innerWidth;

      popover.removeClass('show');

      if (popoverLeft + 340 * 2 > screenWidth) {
        popoverById.addClass('right show');
        popoverById.css({
          top: popoverTop - carHaltHeight + 'px',
          right: screenWidth - popoverLeft + cardRect.width + 'px',
        });
      } else {
        popoverById.removeClass('right').addClass('show');
        popoverById.css({
          top: popoverTop - carHaltHeight + 'px',
          left: popoverLeft + 'px',
          marginLeft: '20px',
        });
      }
    }
  });

  let timeout;

  popover.mouseenter(function () {
    clearTimeout(timeout);
    $(this).addClass('show');
  });

  popover.mouseleave(function () {
    const $this = $(this);
    timeout = setTimeout(function () {
      if (!$this.is(':hover') && !$('.card:hover').length) {
        $this.removeClass('show');
      }
    }, 50);
  });

  cards.mouseleave(function () {
    const popoverShow = $('.popover.show');
    timeout = setTimeout(function () {
      if (!popoverShow.is(':hover') && !$('.card:hover').length) {
        popoverShow.removeClass('show');
      }
    }, 50);
  });
});
