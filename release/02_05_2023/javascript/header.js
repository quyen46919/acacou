$(document).ready(function () {
  const headerMenu = $('.header__menu');
  const drawer = $('.drawer');
  const drawerLevel2 = $('.drawer__level-2');
  const drawerSideBtn = $('.drawer__side-btn');
  const drawerLevel2Btn = $('.drawer__level-2--btn');
  const drawerClose = $('.drawer__close');
  const drawerOverlay = $('.drawer__overlay');
  const search = $('.header__search');
  const searchIcon = $('.header__search-icon');
  const searchInput = $('.header__search-input');
  const searchResult = $('.header__search-result');
  const searchClose = $('.header__search-close');

  const handleChangeOverflowBody = (overflow) => $(document.body).css('overflow', `${overflow}`);

  // Category
  $('.header__category-one .header__category-item').mouseenter(function () {
    const menuOneId = $(this).attr('data-menu-one');

    $('.header__category-two').removeClass('show');
    $('.header__category-three').removeClass('show');
    $(`.header__category-two[data-menu-one="${menuOneId}`).addClass('show');
  });

  $('.header__category-two').on('mouseenter', '.header__category-item', function () {
    const menuTwoId = $(this).attr('data-menu-two');

    $('.header__category-three').removeClass('show');
    $(`.header__category-three[data-menu-two="${menuTwoId}`).addClass('show');
  });
  // Drawer
  drawerSideBtn.each((index, drawerSideBtn) => {
    $(drawerSideBtn).click(function () {
      const drawerTwoId = $(this).attr('data-drawer');
      drawerLevel2.removeClass('show');
      $(`.drawer__level-2[data-drawer=${drawerTwoId}]`).addClass('show');

      drawer.addClass('overflow');
      $('.drawer__side')[0].scrollIntoView();
    });
  });

  drawerLevel2Btn.click(() => {
    drawerLevel2.removeClass('show');
    drawer.removeClass('overflow');
  });

  headerMenu.click(() => {
    handleChangeOverflowBody('hidden');
    drawer.addClass('show');
  });
  drawerClose.click(() => {
    handleChangeOverflowBody('visible');
    drawer.removeClass('show');
  });
  drawerOverlay.click(() => {
    handleChangeOverflowBody('visible');
    drawer.removeClass('show');
  });

  // Search
  searchIcon.click(() => {
    handleChangeOverflowBody('hidden');
    search.addClass('mobile');
    search.addClass('mobile');
    searchInput.focus();
  });

  searchClose.click(() => {
    handleChangeOverflowBody('visible');
    search.removeClass('mobile');
    searchResult.removeClass('show');
  });

  searchInput.on('keyup', function () {
    $.trim($(this).val()) ? searchResult.addClass('show') : searchResult.removeClass('show');
  });

  $(window).on('resize', () => {
    if ($(window).width() <= 768) searchResult.removeClass('show');
  });

  $(window).on('click', () => searchResult.removeClass('show'));

  search.on('click', (e) => e.stopPropagation());
});
