$(document).ready(function () {
  const tabBtns = $('.my-learning__tab-button');
  const btnOptions = $('.my-learning__btn-option');
  const btnOptionIcons = $('.my-learning__btn-option-icon');
  const btnCloses = $('.my-learning__btn-option-close');
  const btnLike = $('.my-learning__card-thumbnail-btn-like');
  const btnTools = $('.my-learning__tools-frequency .my-learning__tools-form-item');
  var inputCreateTitles = $('.my-learning__course-input-create');
  var inputCreateContents = $('.my-learning__course-count-text');

  $('.my-learning__tabs-container').on('scroll', function () {
    const scrollableDiv = $(this)[0];
    Math.ceil(scrollableDiv.scrollLeft + scrollableDiv.clientWidth) >= scrollableDiv.scrollWidth
      ? $(scrollableDiv.parentNode).addClass('hidden')
      : $(scrollableDiv.parentNode).removeClass('hidden');
  });

  tabBtns.each((index, tabBtn) => {
    $(tabBtn).click(function () {
      const tabId = $(this).attr('data-tab');

      tabBtns.each((index, tabBtn) => $(tabBtn).removeClass('active'));

      $(this).addClass('active');
      $('.my-learning__tab-content').each((index, tabContent) => $(tabContent).removeClass('active'));
      $(`.my-learning__tab-content[data-tab="${tabId}"]`).addClass('active');
    });
  });

  btnOptions.each((index, btnOption) => {
    $(btnOptionIcons[index]).click(function (e) {
      e.stopPropagation();
      btnOptions.each((index, btnOption) => $(btnOption).removeClass('active'));

      $(btnOption).toggleClass('active');
    });

    btnCloses.each((index, btnClose) => $(btnClose).click(() => $(btnOption).removeClass('active')));

    $(window).click((e) => {
      var isPopperElement =
        $(e.target).hasClass('.my-learning__option-popper') ||
        $(e.target).closest('.my-learning__option-popper').length > 0;

      if (!isPopperElement) {
        $(btnOption).removeClass('active');
      }
    });
  });
  btnLike.click(() => btnLike.toggleClass('active'));

  btnTools.each((index, btnTool) => {
    $(btnTool).click(function (e) {
      e.stopPropagation();

      const toolId = $(this).attr('data-frequency');
      const labelId = $(this).attr('data-label');

      $('.my-learning__tools-form-list-sub').removeClass('active');
      $(`.my-learning__tools-form-list-sub[data-frequency="${toolId}"]`).addClass('active');

      $('.my-learning__tools-form-end').toggleClass('hidden', labelId === 'once');

      if (!$(btnTool).find('input').is(':checked'))
        $('.my-learning__tools-form-list-sub input').val('').prop('checked', false);
    });
  });

  inputCreateTitles.on('input', function (e) {
    const maxChars = $(this).data('max-chars');
    const inputLength = $(this).val().length;
    let remainingChars = maxChars - inputLength;

    remainingChars = Math.max(remainingChars, 0);

    $(this).siblings('.my-learning__course-count-text').text(remainingChars);

    if (inputLength >= maxChars) $(this).val($(this).val().substring(0, maxChars));
  });
});
