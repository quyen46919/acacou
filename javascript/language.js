$(document).ready(function () {
  const languageListDrawer = $('.language-list__drawer');
  const languageBtns = $('.language-list__button');
  const btnDrawerLanguage = $('.drawer__btn-language');

  btnDrawerLanguage.click(() => languageListDrawer.addClass('show'));

  languageBtns.each((i, languageBtn) => {
    $(languageBtn).click(function () {
      languageBtns.removeClass('active');

      $(this).addClass('active');
    });
  });
});
